#!/bin/bash
# script que permite la conexión a proxmox usando tickets
# version 1.3

# needs pve-manager >= 3.1-44
	rm ~/spiceproxy

#res=$(yad --width=400 --center --title="" --text="Virtual Desktop Infraestructure" \
#--image=dialog-password \
#--form \
#--field="Usuario" \
#--field="Contraseña":H \
#--field="VMID")

#ret=$?
#if [ $ret -eq 0 ]
#then

#$1 = IP o nombre dominio del servidor pve al que conectar
#$2 = ID a asignar a la MV 
#$3 = Nombre del nodo del servidor proxmox  (pve3 o pve4)

	PROXY="$1"
	VMID="$2"

	echo "Intentando conectar con la MV clonada (conexion_mv.sh) ..."
	echo "nodo=$1  -  VMID=$2   -  pve=$3"

	USERNAME="pasarela"
	PASSWORD="e2ah4a"
	USERNAME="${USERNAME}@pve"

	if [ $3 -eq 3 ]; then
		NODE="pve3"
	elif [ $3 -eq 4 ]; then
		NODE="pve4"
	elif [ $3 -eq 8 ]; then
		NODE="pve8"
	else
		zenity --width=420 --height=100 --error --title "PCs remotos" --text "Fallo al conectar con la Máquina remota. Error 101. Avise al Coordinador TIC."
		exit 2
	fi	

	DATA="$(curl -f -s -S -k --data-urlencode "username=${USERNAME}" --data-urlencode "password=${PASSWORD}" "https://${PROXY}:8006/api2/json/access/ticket")"

	echo "AUTH OK"

	TICKET="${DATA//\"/}"
	TICKET="${TICKET##*ticket:}"
	TICKET="${TICKET%%,*}"
	TICKET="${TICKET%%\}*}"

	CSRF="${DATA//\"/}"
	CSRF="${CSRF##*CSRFPreventionToken:}"
	CSRF="${CSRF%%,*}"
	CSRF="${CSRF%%\}*}"

	echo "Proxy=$PROXY   -    NODE=$NODE  -  VMID=$VMID"
	curl -f -s -S -k -b "PVEAuthCookie=${TICKET}" -H "CSRFPreventionToken: $CSRF" "https://${PROXY}:8006/api2/spiceconfig/nodes/${NODE}/qemu/${VMID}/spiceproxy" -d "proxy=${PROXY}" > ~/spiceproxy

	exec remote-viewer --full-screen ~/spiceproxy 
	#remote-viewer spiceproxy --full-screen
