#!/bin/bash
# v1.7b

PORT_PVE=4999

RED=$(ip r show | grep " src "| cut -d " " -f 12 | awk -F. '{print $1 $2}' | head -n 1)
#Lubuntu 18.04#RED=$(ip r show | grep " src "| cut -d " " -f 9 | awk -F. '{print $1 $2}' | head -n 1)
PORT_VM=$(ip r show | grep " src "| cut -d " " -f 12 | awk -F. '{print $3 $4}' | head -n 1)
#Lubuntu 18.04#PORT_VM=$(ip r show | grep " src "| cut -d " " -f 12 | awk -F. '{print $3 $4}' | head -n 1)

#si usamos lubuntu 18.04 debemos cambiar la columna que nos da la red
if [ -z "$RED" ]; then
	RED=$(ip r show | grep " src "| cut -d " " -f 9 | awk -F. '{print $1 $2}' | head -n 1)
	PORT_VM=$(ip r show | grep " src "| cut -d " " -f 9 | awk -F. '{print $3 $4}' | head -n 1)
fi

#Para saber a que servidor PVE debe conectarse el cliente se mira en un fich.
#Si el fichero no existe se avisa de que el PC no está configurado.
FILE="/usr/share/pm/pmclient/pm/.pveserver"

if [ -f $FILE  ]; then
	PVE_SERVER=$(cat $FILE)
else
	#quitar cuando se usen ya los 2 ficheros de config.
	FILE="/usr/share/pm/.pveserver"
	if [ -f $FILE  ]; then
		PVE_SERVER=$(cat $FILE)

	else
		echo "El PC cliente no está configurado. Avise al responsable de mantenimiento."
	#	zenity --width=420 --height=100 --error --title "PCs remotos" --text "El PC cliente no está configurado. Avise al responsable de mantenimiento."
	#	exit 1
	fi
fi

#Para identificar al PC y usuario para usar Windows Server se mira en un fich.
#Si el fichero no existe se avisa de que el PC no está configurado.
FILE="/usr/share/pm/pmclient/pm/.puestopc"
if [ -f $FILE  ]; then
	PUESTO_PC=$(cat $FILE)
else
	echo "El PC cliente no está configurado. Avise al responsable de mantenimiento."
#	zenity --width=420 --height=100 --error --title "PCs remotos" --text "El PC cliente no está configurado. Avise al responsable de mantenimiento."
#	exit 1
fi

if [ -z $RED ]; then
	zenity --width=420 --height=100 --error --title "PCs remotos" --text "No hay conexión de red. Revise las conexiones."
	exit 1
fi
	
# Reajustamos el PORT_VM para este dentro del rango de puertos usables: 1024-49151
#echo ${#cadena} cuenta el num de caracteres
if [ $PORT_VM -lt 1024 ]; then
	PORT_VM=$(expr $PORT_VM + 1024)
elif [ $PORT_VM -gt 49152 ]; then
	PORT_VM=$(echo $PORT_VM  | cut -c1-5)
fi

# averiguamos en que red está conectado el cliente
# y según su hostname obtenemos la IP del servidor PVE
# al que debemos conectarnos
if [ $RED == "192168" ]; then
	if [ $PVE_SERVER -eq 3 ]; then
		IP_PVE=192.168.8.11
	elif [ $PVE_SERVER -eq 4 ]; then
		IP_PVE=192.168.8.12
	else
		zenity --error --title "PCs remotos" --text "PC servidor no identificable. Avise al Coordinador TIC"
		exit 2
	fi	
else
	if [ $PVE_SERVER -eq 3 ]; then
		IP_PVE=172.16.255.3
	elif [ $PVE_SERVER -eq 4 ]; then
		IP_PVE=172.16.255.4
	else
		zenity --error --title "PCs remotos" --text "PC servidor no identificable. Avise al Coordinador TIC"
		exit 2
	fi	
fi

/usr/share/pm/pmclient/pm/conexion_mv.sh $IP_PVE $PORT_VM $PVE_SERVER
#spicy --full-screen=auto-conf -h $IP_PVE -p $PORT_VM
#spicy  -h $IP_PVE -p $PORT
