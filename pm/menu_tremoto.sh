#!/bin/bash
# Script que muestra el menú de SSOO disponibles
# y realiza la conexión con el servidor PVE adecuado
# teniendo en cuenta en que red está conectado.

############################################################################
# LISTA DE ESTADOS DE EXIT:
############################################################################
# 0   - Ok
# 1   - No se ha podido averiguar a que red está conectado el PC cliente
# 2   - No se ha podido averiguar a que servidor PVE conectarse
# 3   - Error en la opción seleccionada en el menú

############################################################################
# Inicialización de variables configurables
############################################################################

#Version para un solo servidor Proxmox
VERSION="2.8.2"

RUTA="/usr/share/pm/pmclient/pm"
#RUTA="/home/oscar/Escritorio/pm"
FILE_PVESERVER="${RUTA}/.pveserver"
FILE_PUESTO="${RUTA}/.puestopc"

#puerto servidor PVE donde está instalado el servidor tcp para recibir los datos de clonación
PORT_PVE=4999

RED=$(ip r show | grep " src "| cut -d " " -f 12 | awk -F. '{print $1 $2}' | head -n 1)
PORT_VM=$(ip r show | grep " src "| cut -d " " -f 12 | awk -F. '{print $3 $4}' | head -n 1)


############################################################################
# Inicio programa
############################################################################

sleep 2s | zenity --width=420 --height=100 --progress --pulsate --auto-close --text "Iniciando programa. Espere por favor..." --title "Inicio del sistema remoto (v.${VERSION})"

echo "***1 --> PORT_VM=$PORT_VM ***"


#si usamos lubuntu 18.04 debemos cambiar la columna que nos da la red
if [ -z "$RED" ]; then
#RED=$(ip r show | grep " src "| cut -d " " -f 9 | awk -F. '{print $1 $2}' | head -n 1)
	RED=$(ip r show | grep " src "| grep " metric " | cut -d " " -f 9 | awk -F. '{print $1 $2}' | head -n 1)
#PORT_VM=$(ip r show | grep " src "| cut -d " " -f 12 | awk -F. '{print $3 $4}' | head -n 1)
#PORT_VM=$(ip r show | grep " src "| grep -v linkdown | cut -d " " -f 12 | awk -F. '{print $3 $4}')
	PORT_VM=$(ip r show | grep " src "| grep " metric " | cut -d " " -f 9 | awk -F. '{print $3 $4}' | head -n 1)
fi

echo "***2 --> PORT_VM=$PORT_VM ***"

#si usamos raspberrypi debemos cambiar la columna que nos da la red
if [ -z "$PORT_VM" ]; then
	RED=$(ip r show | grep " src "| cut -d " " -f 7 | awk -F. '{print $1 $2}' | head -n 1)
	PORT_VM=$(ip r show | grep " src "| cut -d " " -f 7 | awk -F. '{print $3 $4}' | head -n 1)
fi

echo "***3 --> PORT_VM=$PORT_VM ***"

if [ -z $RED ]; then
	zenity --width=420 --height=100 --error --title "PCs remotos" --text "No hay conexión de red. Revisa las conexiones e inténtalo de nuevo. Si sigue sin funcionar avise a su profesor para que cree una incidencia de mantenimiento."
	exit 1
fi

#Para saber a que servidor PVE debe conectarse el cliente se mira en un fich.
#Si el fichero no existe se avisa de que el PC no está configurado.
if [ -f $FILE_PVESERVER  ]; then
	PVE_SERVER=$(cat $FILE_PVESERVER)
else
	zenity --width=420 --height=100 --error --title "PCs remotos" --text "El PC cliente no está configurado. Avise a su profesor para que cree una incidencia de mantenimiento."
	exit 1
fi

echo "***4 --> PVE_SERVER=$PVE_SERVER ***"


#Para identificar al PC y usuario para usar en S.O. Linux / Windows por RDP se mira en un fich.
#Si el fichero no existe se avisa de que el PC no está configurado.
if [ -f $FILE_PUESTO  ]; then
	PUESTO_PC=$(cat $FILE_PUESTO)
else
	zenity --width=420 --height=100 --error --title "PCs remotos" --text "El PC cliente no está configurado. Avise a su profesor para que cree una incidencia de mantenimiento."
	exit 2
fi

	
# Reajustamos el PORT_VM para que este dentro del rango de puertos usables: 1024-49151
#echo ${#cadena} cuenta el num de caracteres
if [ $PORT_VM -lt 1024 ]; then
	PORT_VM=$(expr $PORT_VM + 1024)
elif [ $PORT_VM -gt 49152 ]; then
#	PORT_VM=$(echo $PORT_VM | rev | cut -c1-5 | rev)
	PORT_VM=$(echo $PORT_VM | cut -c1-5)
fi
echo "*** PORT_VM=$PORT_VM ***"

# Segun el aula al que está conectado el cliente
# obtenemos la IP del servidor PVE
# al que debemos conectarnos
if [ $PVE_SERVER -eq 3 ]; then
	IP_PVE="172.20.250.103"
elif [ $PVE_SERVER -eq 4 ]; then
	IP_PVE="172.16.255.4"
else
	zenity --width=420 --height=100 --error --title "PCs remotos" --text "PC servidor no identificable. Avise a su profesor para que cree una incidencia de mantenimiento."
	exit 2
fi	

#Servidor PVE
#PVE_SERVER=8

#IP del servidor Proxmox
#IP_PVE="172.20.250.108"


# *****************************************************   MENU  ************************************************************
w=$(xdpyinfo | awk '/dimensions/{print $2}'|cut -f1 -dx)
h=$(xdpyinfo | awk '/dimensions/{print $2}'|cut -f2 -dx)
SO=$(zenity --width=$w --height=$h \
			--list \
			--title "PCs remotos - v${VERSION}  (Servidor: ${PVE_SERVER} - ${PORT_VM} - Puesto: ${PUESTO_PC})" \
			--column "Elige el Sistema Operativo a utilizar:" "Linux-2022" "Windows-2022" "Linux-2024" "Windows-2024" "Linux23-Individual" "Pruebas1"  \
			)
			
if [ $? -eq 0 ]; then

	case $SO in
		 "Windows-2024")  
				# RDP con windows server
				IP_SERVIDOR="172.16.0.9"

				echo "--> Conectando por rdp al equipo Windows ${IP_SERVIDOR}"

				#para usar el comando xfreerdp hay que instalar en el pc cliente el paquete: freerdp2-x11
				#xfreerdp /sound /f  /u:"puesto${PUESTO_PC}" /p:"puesto${PUESTO_PC}" /cert-name:"w2019-IESJV" /cert:tofu /v:${IP_SERVIDOR}:3389
				xfreerdp /sound /f  /u:"puesto${PUESTO_PC}" /p:"puesto${PUESTO_PC}" /cert-name:"w2019-IESJV" /cert:ignore /v:${IP_SERVIDOR}:3389
				;;
		
		"Linux-2024")
				# RDP con Linux
				IP_SERVIDOR="172.20.250.4"

				echo "--> Conectando por rdp al equipo Linux ${IP_SERVIDOR}"
				xfreerdp /sound /f /u:"puesto${PUESTO_PC}" /p:"puesto${PUESTO_PC}"  /v:${IP_SERVIDOR}:3389
				;;
		
		"Pruebas1")
				# RDP con Linux
				IP_SERVIDOR="172.16.0.10"

				echo "--> Conectando por rdp al equipo ${IP_SERVIDOR}"
				xfreerdp /sound /f /u:"puesto${PUESTO_PC}" /p:"puesto${PUESTO_PC}"  /v:${IP_SERVIDOR}:3389
				;;
				
		 "Windows-2022")  
				#Para saber a que servidor PVE debe conectarse el cliente se mira en un fich.
				#Si el fichero no existe se avisa de que el PC no está configurado.
				if [ -f $FILE_PVESERVER  ]; then
					PVE_SERVER=$(cat $FILE_PVESERVER)
				else
					zenity --width=420 --height=100 --error --title "PCs remotos" --text "El PC cliente no está configurado. Avise a su profesor para que cree una incidencia de mantenimiento."
					exit 1
				fi

				# RDP con windows server

				#if [ $RED == "192168" ]; then # red de la junta
				if [ $PVE_SERVER -eq 3 ]; then
					IP_SERVIDOR="172.20.250.3"
				else
					IP_SERVIDOR="172.16.20.5"
				fi
				echo "--> Conectando por rdp al equipo Windows ${IP_SERVIDOR}"
				xfreerdp /sound /f  /u:"puesto${PUESTO_PC}" /p:"puesto${PUESTO_PC}" /cert-name:"w2019-IESJV" /cert:ignore /v:${IP_SERVIDOR}:3389
				#xfreerdp /sound /f  /u:"puesto${PUESTO_PC}" /p:"puesto${PUESTO_PC}" /cert-name:"w2019-IESJV" /cert:tofu /v:${IP_SERVIDOR}:3389
				;;
		
		"Linux-2022")
				#Para saber a que servidor PVE debe conectarse el cliente se mira en un fich.
				#Si el fichero no existe se avisa de que el PC no está configurado.
				if [ -f $FILE_PVESERVER  ]; then
					PVE_SERVER=$(cat $FILE_PVESERVER)
				else
					zenity --width=420 --height=100 --error --title "PCs remotos" --text "El PC cliente no está configurado. Avise a su profesor para que cree una incidencia de mantenimiento."
					exit 1
				fi
		
				# RDP con Linux
				if [ $PVE_SERVER -eq 3 ]; then
					IP_SERVIDOR="172.20.250.5"
				else
					#VLAN-LAN:
					IP_SERVIDOR="172.20.250.6"
				fi
				#xfreerdp /f /u:"puesto19h" /p:"puesto19h" /v:${IP_SERVIDOR}:3389
				echo "--> Conectando por rdp al equipo Linux ${IP_SERVIDOR}"
				xfreerdp /sound /f /u:"puesto${PUESTO_PC}" /p:"puesto${PUESTO_PC}"  /v:${IP_SERVIDOR}:3389
				;;				



		 *) 
				echo "Opcion no válida"
				#Queremos clonar una MV de una plantilla
				/usr/bin/expect ${RUTA}/telnet.exp $IP_PVE $PORT_PVE $SO $PUESTO_PC
				${RUTA}/conexion_mv.sh $IP_PVE $PORT_VM $PVE_SERVER
	esac
fi
